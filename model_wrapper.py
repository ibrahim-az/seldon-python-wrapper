import numpy as np
import seldon_core
from seldon_core.user_model import SeldonComponent
from typing import Dict, List, Union, Iterable
import os
import dill
import logging

logger = logging.getLogger(__name__)


class ModelWrapper(SeldonComponent):
    def __init__(self, model_uri: str = None, method: str = "predict_proba", model_file: str = "sample.model"):
        super().__init__()
        self.model_uri = model_uri
        self.method = method
        self.model_file = model_file
        self.ready = False
        logger.info(f"Model uri: {self.model_uri}")
        logger.info(f"method: {self.method}")
        logger.info(f"model file : {self.model_file}")
        self.load()

    def load(self):
        model_file = os.path.join(
            seldon_core.Storage.download(self.model_uri), self.model_file
        )        
        self._model = dill.load(model_file)
        self.ready = True

    def predict(
        self, X: np.ndarray, names: Iterable[str], meta: Dict = None
    ) -> Union[np.ndarray, List, str, bytes]:
        if not self.ready:
            self.load()
        if self.method == 'predict_proba':
            result: np.ndarray = self._model.predict_proba(X)
            return result
        else:
            result: np.ndarray = self._model.predict(X) 
            return result
